## Gulp

### Gulp 的基本使用
1. yarn init 初始化项目
2. yarn add gulp --dev 作为开发依赖安装到项目
3. 新建gulp的入口文件gulpfile.js
4. exports.任务名 = 函数 => {  任务执行完毕需要调用 形参()   }
5. exports.default 为默认任务
```javascript
// glup入口文件
// glup入口文件
exports.foo = done =>{
    console.log('foo task working~');
    done() //标识任务完成
}

exports.default = done => {
    console.log("默认任务")
    done();
}
```
6. 执行任务 yarn gulp 任务名 `default 任务默认执行不用加任务名字`

7. 这种方式也可以
```javascript
// glup入口文件

const gulp = require("gulp");

gulp.task("bar", done => {
    console.log("另一种方式")
    done();
})
```
### Gulp 的组合任务
1. 定义多个任务函数
2. 利用`series`方法执行多个任务  串行执行
3. 利用``方法执行多个任务 并行执行（需要互补干扰的任务可以这样执行）
```javascript
const task1 = done => {
    setTimeout(() => {
        console.log("任务1执行");
        done();
    }, 1000);
}

const task2 = done => {
    setTimeout(() => {
        console.log("任务2执行");
        done();
    }, 1000);
}

const task3 = done => {
    setTimeout(() => {
        console.log("任务3执行");
        done();
    }, 1000);
}

//串行
exports.foo = series(task1,task2,task3);
//并行执行
exports.bar = parallel(task1,task2,task3);
```


### Gulp 的异步任务
1. 给回调函数传入一个错误对象
2. 使用promise
3. stream对象


```javascript
const fs = require("fs");

exports.callback = done => {
    console.log("回调函数");
    done();
}

exports.callback_error = done => {
    console.log("回调函数执行失败")
    done(new Error("任务失败"))
}

exports.promise = done => {
    return Promise.resolve();//成功 不需要任何值 gulp会忽略
}


exports.promise_error = done => {
    return Promise.reject(new Error("任务失败"));//失败
}

const timeout = time => {
    return new Promise(resolve => {
        setTimeout(resolve, time);
    })
}

exports.async = async () => {
    await timeout(1000);
}




exports.stream = () => {
    const readStream = fs.createReadStream('package.json')
    const writeStream = fs.createWriteStream('temp.txt');
    readStream.pipe(writeStream);
    return readStream
}

exports.stream1 = done => {
    const readStream = fs.createReadStream('package.json')
    const writeStream = fs.createWriteStream('temp.txt');
    readStream.pipe(writeStream);
    readStream.on('end',()=>{
        done();
    })
}
```


###  Gulp 构建过程核心工作原理
1. 读取流    =======> 输入
2. 转换流    =======>加工
3. 写入流    =======>输出
基于流的方式

###  Gulp 文件操作 API

1. 可以使用通配符
2. 使用src方法读取流
3. gulp-clean-css 插件转换流 压缩代码
4. gulp-rename 插件转换流 重命名
5. dest方法写入流
```javascript
const { src, dest } = require('gulp')
const cleanCSS = require('gulp-clean-css')
const rename = require('gulp-rename')

exports.default = () => {
  return src('src/*.css')
    .pipe(cleanCSS())
    .pipe(rename({ extname: '.min.css' }))
    .pipe(dest('dist'))
}
```

###  Gulp 案例 - 样式编译
1. gulp-sass 会忽略下划线开头的文件明
2. gulp-sass outputStyle: 'expanded' 完全展开css
3. base的目的是保持目录结构

```javascript
const { src, dest} = require('gulp')
const sass = require('gulp-sass')
const style = () => {
  return src('src/assets/styles/*.scss', { base: 'src' })
    .pipe(sass({ outputStyle: 'expanded' }))
    .pipe(dest('dist'))
}
```

### Gulp 案例 - 脚本编译
1. gulp-babel  需要  @babel/core  @babel/preset-env(ECAM新特性会转换)
2. 不传入presets: ['@babel/preset-env'] 会导致转换貌似没有效果 因为babel只是ECMA的转换平台具体转换的是preset-env

```javascript
const babel = require("gulp-babel")
const script = () => {
  return src('src/assets/scripts/*.js', { base: 'src' })
    .pipe(babel({ presets: ['@babel/preset-env'] }))
    .pipe(dest('dist'))
}
```


### Gulp 案例 - 页面模板编译
1. gulp-swig 处理模板，`如果不只是src下面的主目录还有子文件夹需要/**/*.html` 表示任意子目录

```javascript
const swig = require("gulp-swig");
const data = {你的数据}
const page = () => {
  return src('src/*.html', { base: 'src' })
    .pipe(swig({ data, defaults: { cache: false } })) // 防止模板缓存导致页面不能及时更新
    .pipe(dest('dist'))
}
```
2. 组合以上任务 应为 以上任务没有关联可以并行执行
```javascript
const {parallel} = require("gulp");
const	compile = parallel(style,script,page)
module.exports = {
	compile
}
```
### Gulp 案例 - 图片和字体文件转换
#### 图片
1. gulp-imagemin c++模块 需要下载2进制集  下载慢 有可能会出错
2. 会告诉压缩比例（无损压缩）

```javascript
const imagemin = require("gulp-imagemin")
const image = () => {
  return src('src/assets/images/**', { base: 'src' })
    .pipe(imagemin())
    .pipe(dest('dist'))
}
module.exports = {
	imagemin
}
```

#### 字体
```javascript
const font = () => {
  return src('src/assets/fonts/**', { base: 'src' })
    .pipe(imagemin())
    .pipe(dest('dist'))
}
```
#### 其他文件拷贝
- 因为public 定义的为不做任何处理 所以直接拷贝
```javascript
const extra = () => {
  return src('public/**', { base: 'public' })
    .pipe(dest('dist'))
}
```
#### 任务组合使条例更清晰
```javascript
	const compile = parallel(style,script,page,image,font)
	const build = parallel(compile,extra)
```


### Gulp 案例 - 其他文件及文件清除
1. clear 需要在build之前 不再之前的话 有可能会删掉已经build生成文件被删除的问题
```javascript
const {series} = require("gulp")
const del = require("del");

const clear = () => {
	return del(['dist'])
}

const build = series(clear,parallel(compile,extra))
```

### Gulp 案例 - 自动加载插件
1. 随着构建任务复杂了 引入插件太多  gulp-load-plugins 自动加载模块
2. 命名去掉-变成托峰命名法
3. 直接用plugins.模块名字 来使用
```javascript
const loadPlugins = require("gulp-load-plugins")
const plugins = loadPlugins()
//例:
plugins.imagemin();
```

### Gulp 案例 - 开发服务器
1. 安装 `browser-sync`  --------  yarn add browser-sync --dev 支持热更新
```javascript
const browserSync = require("browser-sync")
const bs = browserSync.create()//创建开发服务器


//定义任务启动开发服务器
const server = () => {
	bs.init({
		notify:false,//提示
		port: 9999,//端口
		open: false,//自动打开浏览器
		files:'dist/*', //被监听的文件通配符
		server:{
			baseDir: 'dist' //网站根目录
			//定义路由
			routes:{
				'/node_modules':'/node_modules'
			}
		}
	})
}

module.exports = {
	server
}
```

### Gulp 案例 - 监视变化以及构建优化
- gulp watch api监听
- watch是方法接收一个路径通配符
- 这里可能会因为swig模板引擎的缓存机制导致页面不会变化，此时需要额外将swig选项中的cache设置为false
- 图片和字体和额外文件监视没有意义 开发不监视
- baseDir为数组就是在前一个找不到就在后一个去寻找  开发不处理图片等文件 直接请求源文件

```javascript
const server = () => {
  watch('src/assets/styles/*.scss', style)
  watch('src/assets/scripts/*.js', script)
  watch('src/*.html', page)
	bs.init({
		notify:false,//提示
		port: 9999,//端口
		open: false,//自动打开浏览器
		files:'dist/*', //被监听的文件通配符
		server:{
			baseDir: ['dist','src','public'] //网站根目录
			//定义路由
			routes:{
				'/node_modules':'/node_modules'
			}
		}
	})
}
```

serve命令之前必须编译一次模板js等 所以在包裹一层
```javascript
const compile = parallel(style, script, page)

// 上线之前执行的任务
const develop = series(compile, serve)
```

图片等变化时还想自动更新到浏览器
```javascript
watch([
    'src/assets/images/**',
    'src/assets/fonts/**',
    'public/**'
  ], bs.reload)
```

可以去掉files 在任务后面推到浏览器更新 bs.reload({ stream: true }
```javascript
const style = () => {
  return src('src/assets/styles/*.scss', { base: 'src' })
    .pipe(plugins.sass({ outputStyle: 'expanded' }))
    .pipe(dest('temp'))
    .pipe(bs.reload({ stream: true }))
}
```

###  Gulp 案例 - useref 文件引用处理
- 在node_modules 包文件处理  `useref` 处理
- 注释  注意定义在引入文件位置 ，结束需要
	示例： 
```html
    <!-- build:css assess/css/style.css -->
	
	<!--endbuild -->
```
1. 安装 `gulp-useref`  yarn add gulp-useref --dev
2. 创建任务

```javascript
const useref = () => {
	return src("dist/*.html ",{base: })
	.pipe(pulgins.useref({searchPath:['dist','.']}))
	.pipe(dest('dist'))
}
``` 



### Gulp 案例 - 文件压缩

1. 安装 `gulp-htmlmin,gulp-uglify gulp-clear-css` 压缩html js css
2. 安装 `gulp-if`插件判断是js还是css还是html
3. html 压缩只是删除多余空格 collapseWhitespace 删除换行
```javascript
const useref = () => {
	return src("dist/*.html ",{base: })
	.pipe(pulgins.useref({searchPath:['dist','.']}))
	.pipe(pulgins.if(/\.js$/,pulgins.uglify())) //压缩 js
	.pipe(pulgins.if(/\.js$/,pulgins.clearCss())) //压缩 css
	.pipe(pulgins.if(/\.js$/,pulgins.htmlmin({
		collapseWhitespace: true,
		minIfyCSS: true, //压缩页面上的css
        minifyJS: true, //压缩页面上的js
     }))) //压缩 html
	.pipe(dest('te')) //新目录 dist 会冲突
}
```
###  Gulp 案例 - 重新规划构建过程
1. 清空目录加上temp

```javascript
const clean = () => {
  return del(['dist', 'temp'])
}
```
2. 把开始js css html处理过程放入temp目录下面
```javascript
const style = () => {
  return src('src/assets/styles/*.scss', { base: 'src' })
    .pipe(plugins.sass({ outputStyle: 'expanded' }))
    .pipe(dest('temp'))
    .pipe(bs.reload({ stream: true }))
}

const script = () => {
  return src('src/assets/scripts/*.js', { base: 'src' })
    .pipe(plugins.babel({ presets: ['@babel/preset-env'] }))
    .pipe(dest('temp'))
    .pipe(bs.reload({ stream: true }))
}

const page = () => {
  return src('src/*.html', { base: 'src' })
    .pipe(plugins.swig({ data, defaults: { cache: false } })) // 防止模板缓存导致页面不能及时更新
    .pipe(dest('temp'))
    .pipe(bs.reload({ stream: true }))
}
```
3. serve 修改baseDir dist 为 temp
```javascript
const serve = () => {
  watch('src/assets/styles/*.scss', style)
  watch('src/assets/scripts/*.js', script)
  watch('src/*.html', page)
  // watch('src/assets/images/**', image)
  // watch('src/assets/fonts/**', font)
  // watch('public/**', extra)
  watch([
    'src/assets/images/**',
    'src/assets/fonts/**',
    'public/**'
  ], bs.reload)

  bs.init({
    notify: false,
    port: 2080,
    // open: false,
    // files: 'dist/**',
    server: {
      baseDir: ['temp', 'src', 'public'],
      routes: {
        '/node_modules': 'node_modules'
      }
    }
  })
}
```
4. useref任务修改读取目录
```javascript
const useref = () => {
  return src('temp/*.html', { base: 'temp' })
    .pipe(plugins.useref({ searchPath: ['temp', '.'] }))
    // html js css
    .pipe(plugins.if(/\.js$/, plugins.uglify()))
    .pipe(plugins.if(/\.css$/, plugins.cleanCss()))
    .pipe(plugins.if(/\.html$/, plugins.htmlmin({
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true
    })))
    .pipe(dest('dist'))
}
```

5. 修改 build任务
```javascript
	- useref任务必须要在 compile任务完成之后
const compile = parallel(style, script, page)

// 上线之前执行的任务
const build =  series(
  clean,
  parallel(
    series(compile, useref),
    image,
    font,
    extra
  )
)
```

###  Gulp 案例 - 补充
1. 需要看任务是否需要导出，只导出需要任务
2. 把命令定义到package.json里面 方便使用
```json
{
	"scripts":{
		"clean": "gulp clean",
		"build": "gulp build",
		"dev": "gulp develop"
	}
}
```
3. 如何提取多个项目中共同的构建化问题


# 封装工作流

## 准备

gulpfile + gulp = 构建工作流

gulpfile + gulp Cli  => pages

- 创建git仓库管理

## 封装工作流 - 提取 gulpfile
1. 把所需要的依赖作为构建依赖
2. 把工作流 link 到全局
`mlf-pages: yarn link`
3. link 到项目
`mlf-gulp-demo: yarn link "mlf-pages"`
4. 项目中引入 link的包 
```javascript
module.exports = require("mlf-pages");
```

## 封装工作流 - 解决模块中的问题
1. 在根目录新建pages.config.js 文件用于约定导出一个对象
2. 在工作流index.js  根据使用目录读取相应文件 注意需要处理是否有这个文件状态
3. 使用命令需要安装gulp gulp-cli
```javascript
const cwd = process.cwd();
try{
  const loadConfig = require(`${cwd}/pages.config.js`);
  config = Object.assign({},config,loadConfig)
} catch (err){

}
```
3. 更改相应使用config位置
4. 处理文件的插件需要改为 require()引入
`[require('@babel/preset-env')] `

##  封装工作流 - 抽象路径配置
1. 把写死的文件抽象为可配置
```javascript
const { src, dest, parallel, series, watch } = require('gulp')

const del = require('del')
const browserSync = require('browser-sync')

const loadPlugins = require('gulp-load-plugins')

const plugins = loadPlugins()
const bs = browserSync.create()
const cwd = process.cwd();
let config = {
  //default config
  build:{
    src:'src',
    dist: 'dist',
    temp: 'temp',
    public: 'public',
    paths:{
      styles:'assets/styles/*.scss',
      scripts:'assets/scripts/*.js',
      pages:'*.html',
      images:'assets/images/**',
      fonts:'assets/fonts/**',
    }
  }
}
// const data = require

try{
  const loadConfig = require(`${cwd}/pages.config.js`);
  config = Object.assign({},config,loadConfig)
} catch (err){

}


const clean = () => {
  return del([config.build.dist, config.build.temp])
}

const style = () => {
  return src(config.build.paths.styles, { base:  config.build.src ,cwd: config.build.src})
    .pipe(plugins.sass({ outputStyle: 'expanded' }))
    .pipe(dest(config.build.temp))
    .pipe(bs.reload({ stream: true }))
}

const script = () => {
  return src(config.build.paths.scripts, { base:  config.build.src , cwd: config.build.src })
    .pipe(plugins.babel({ presets: [require('@babel/preset-env')] }))
    .pipe(dest(config.build.temp))
    .pipe(bs.reload({ stream: true }))
}

const page = () => {
  return src(config.build.paths.pages, { base:  config.build.src , cwd: config.build.src })
    .pipe(plugins.swig({ data: config.data, defaults: { cache: false } })) // 防止模板缓存导致页面不能及时更新
    .pipe(dest(config.build.temp))
    .pipe(bs.reload({ stream: true }))
}

const image = () => {
  return src(config.build.paths.images, { base:  config.build.src , cwd: config.build.src })
    .pipe(plugins.imagemin())
    .pipe(dest(config.build.dist))
}

const font = () => {
  return src(config.build.paths.fonts, { base:  config.build.src , cwd: config.build.src })
    .pipe(plugins.imagemin())
    .pipe(dest(config.build.dist))
}

const extra = () => {
  return src('**', { base: config.build.public, cwd: config.build.public })
    .pipe(dest(config.build.dist))
}

const serve = () => {
  watch(config.build.paths.styles, {cwd: config.build.src},style)
  watch(config.build.paths.scripts,{cwd: config.build.src}, script)
  watch(config.build.paths.scripts,{cwd: config.build.src}, page)
  watch([
    config.build.paths.images,
    config.build.paths.fonts,
  ],{cwd: config.build.src}, bs.reload)

  watch("**",{cwd: config.build.public}, bs.reload)

  bs.init({
    notify: false,
    port: 2080,
    // open: false,
    // files: 'dist/**',
    server: {
      baseDir: [config.build.temp, config.build.src, config.build.public],
      routes: {
        '/node_modules': 'node_modules'
      }
    }
  })
}

const useref = () => {
  return src('temp/*.html', { base: config.build.temp })
    .pipe(plugins.useref({ searchPath: [config.build.temp, '.'] }))
    // html js css
    .pipe(plugins.if(/\.js$/, plugins.uglify()))
    .pipe(plugins.if(/\.css$/, plugins.cleanCss()))
    .pipe(plugins.if(/\.html$/, plugins.htmlmin({
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true
    })))
    .pipe(dest(config.build.dist))
}

const compile = parallel(style, script, page)

// 上线之前执行的任务
const build =  series(
  clean,
  parallel(
    series(compile, useref),
    image,
    font,
    extra
  )
)

const develop = series(compile, serve)

module.exports = {
  clean,
  build,
  develop
}

```

## 封装工作流 - 包装 Gulp CLI
创建bin目录并创建mlf-pages.js作为执行文件
```javascript
#!/usr/local/bin/node 
process.argv.push("--cwd")
process.argv.push(process.cwd())
process.argv.push("--gulpfile")
process.argv.push(require.resolve(".."))
require('gulp/bin/gulp')
console.log(process.argv)
```
package.json中添加
```javascript
  "bin": "bin/mlf-pages.js",
```

封装工作流 - 发布并使用模块
yarn publish



###  FIS 的基本使用
特点： 高度集成

默认命令
1. fis3 release 
把路径变为绝对路径

### FIS 编译与压缩
1. 声明式配置 match 第一个参数命中文件
2. 第二个参数配置

fis3 inspect 调试 显示









