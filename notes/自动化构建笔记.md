# 自动化构建任务三：

### 1.  自动化构建简介

一切重复工作本应自动化

- 自动化： 通过机器代替手工完成工作
- 构建： 就是转换，就是把一个东西转换成另外的东西

作用： 脱离运行环境的种种问题，在开发阶段使用提高效率的语法、规范和标准
- ECMAScript next        使用最新标准提高编码效率
- sass                   增强css可编程性
- 模板引擎                抽象重复的html

将不被支持的代码转换为能够执行的代码


### 2. 自动化构建初体验

案例：用sass代替css

npm Scripts
- 来解决需要重复输入命令
- 实现自动化构建最简单的方式
- pre命令  在这个命令之前执行
- --watch 会监听文件变化但是会阻塞npm执行
- npm-run-all 可以执行多个
- --files \"css/*.css\" 监听多个文件并自动刷新浏览器

```json
"scripts": {
    "build": "sass scss/main.scss css/style.css --watch",
    "server": "browser-sync . --files \"css/*.css\"",
    "start": "run-p build server"
  }
```


### 3. 常用的自动化构建工具

npm scripts 可以用于简单的 复杂的就很吃力

常见的自动化构建工具

- grunt 
    最早的前端构建系统，基于临时文件，构建速度比较慢，读写文件次数多
- gulp
    很好的解决了grunt构建速度慢的问题，内存中读写，支持同时执行多个任务，方式通俗易懂
- fis
    百度的前端构建系统，微内核，集成在内部，很轻松处理资源加载，模块化部署，性能优化

### 4.  Grunt 的基本使用
1. 创建package.json文件
2. 创建gruntfile.js
    - 用于定义一些需要 Grunt 自动执行的任务
    - 需要导出一个函数
    - 此函数接收一个grunt 的形参 内部提供一些创建任务时可以用到的api
```javascript
module.exports = grunt => {
    grunt.registerTask('foo',() => {
        console.log('hello grunt~')
    })
    grunt.registerTask('bar','任务描述',() => {
        console.log('other task~')
    })
    // grunt.registerTask('default',() => {
    //     console.log('default task~');
    // })
    grunt.registerTask('default',['foo','bar']);

    //异步任务
    grunt.registerTask('async-task',function(){
        const done = this.async();
        
        setTimeout(() => {
            console.log('async task wroking~');
            //标记任务完成
            done();
        }, 1000);
    })
}

```
3. yarn grunt 执行 也可  yarn grunt [gruntfile文件定义的任务名称]


### 5. Grunt 标记任务失败
- 同步代码 return false 标记失败  失败之后的任务不会再执行 
- 运行 yarn grunt --force 会忽略失败的任务继续执行
- 异步代码执行 this.aysnc() 创建的函数时传入一个false标记失败

```javascript  
module.exports = grunt => {
    grunt.registerTask('aa1',() =>{
        console.log('aa1')
        return false;
    })  
    grunt.registerTask('aa2',() =>{
        console.log('aa2')
    })
    grunt.registerTask('aa3',() =>{
        console.log('aa3')
    })
    grunt.registerTask('aa4',() =>{
        console.log('aa4')
    })
    grunt.registerTask('aa5',() =>{
        const done = this.async();

        setTimeout(() => {
            done(false)
        }, 1000);
    })
    grunt.registerTask('default', ['aa1','aa2','aa3','aa4','aa5']);
}
```


### 6. Grunt 的配置方法
- initConfig 接收一个对象 key一般与任务名称保持一值

- grunt.config 取initConfig创建的值

```javascript
module.exports = grunt => {
    grunt.initConfig({
        foo:{
            bar: 123
        }
    });


    grunt.registerTask('foo',()=>{
        console.log(grunt.config('foo'));
    })
}
```


### 7. Grunt 多目标任务
- registerMultiTask 方法
- grunt.initConfig({registerMultiTask同名的key}) 
- 每个都可以有options


### 8. Grunt 插件的使用
- npm 安装这个插件 yarn add 包名 --dev
- 载入   grunt.loadNpmTasks('包名')
- 配置  yarn.initConfig

```javascript
module.exports = grunt => {
    grunt.initConfig({
        clean:{
            temp:"temp/app.js"
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean')
}
```
### 9. Grunt 常用插件及总结
- grunt sass

    1. grunt-sass 需要 sass支持
    2. 载入任务 grunt.loadNpmTasks('grunt-sass');
    3. initConfig

    ```
    {
        sass:{
            options:{
                sourceMap: true,// 生成sourceMap文件
                implemenetation: sass 
            },
            main: {
                files:{
                    'dist/css/mian.css':'src/scss/main.scss'
                }
            }
        }
    }
    ```
- babel 需要 grunt-babel @babel/core @babel/preset-evn
    1. 安装 yarn add grunt-babel @babel/core @babel/preset-evn --dev
    2. 导入 grunt.loadNpmTasks('grunt-babel');
    3. 配置initConfig
    ```javascript

    ```

- load-grunt-tasks 自动加载所有的 grunt 插件中的任务
```javascript
loadGruntTasks = require('load-grunt-tasks');


loadGruntTasks(grunt)
```

- grunt-contrib-watch
    1. 安装 yarn add grunt-contrib-watch --dev
    2. 导入 grunt.loadNpmTasks('grunt-contrib-watch');
    3. 配置initConfig
```javascript
watch:{
    js: {
        files:['src/js/*.js'],
        tasks:['babel']
    },
    css: {
        files:['src/scss/*.scss'],
        tasks:['scss']
    }
}
```