/*
 * @Descripttion: 
 * @version: 
 * @Author: 马琳峰
 * @Date: 2020-11-02 12:51:26
 * @LastEditors: 马琳峰
 * @LastEditTime: 2020-11-03 14:46:34
 */
$(($) => {
  const $body = $('html, body')

  $('#scroll_top').on('click', () => {
    $body.animate({ scrollTop: 0 }, 600)
    return false
  })
})