
const minimist = require('minimist') // 解析命令行选项
const sass = require('sass');
const args = minimist(process.argv.slice(2)) //解析后的命令行参数
const cwd = process.cwd(); //当前的目录
const browserSync = require("browser-sync")
const bs = browserSync.create()
const loadGruntTasks = require("load-grunt-tasks")
const mozjpeg = require('imagemin-mozjpeg')


let config = {
  //default config
  build:{
    src:'src',
    dist: 'dist',
    temp: 'temp',
    public: 'public',
    paths:{
      styles:'assets/styles/*.scss',
      scripts:'assets/scripts/*.js',
      pages:'*.html',
      images:'assets/images/**',
      fonts:'assets/fonts/',
    }
  }
}

//启动服务器配置
const bsInit = {
    notify: false,
    port: args.port || 2080,
    open: args.open || false,
};

try{
    const loadConfig = require(`${cwd}/pages.config.js`);
    config = Object.assign({},config,loadConfig)
  } catch (err){
  
}
  

module.exports = grunt => {
    
    loadGruntTasks(grunt) //自动加载所有的grunt插件

    grunt.initConfig({
        //清除零食目录
        clean:{
            dist: config.build.dist,
            temp: config.build.temp
        },
       
        //监听js 和 css 改变
        watch: {
            js: {
                //文件地址
                files: [config.build.src + '/' + config.build.paths.scripts],
                //执行的任务
                tasks: ['babel']
            },
            css: {
                //文件地址
                files: [config.build.src + '/' + config.build.paths.styles],
                //执行的任务
                tasks: ['sass'],
            },
            html: {
                //文件地址
                files: [config.build.src + '/' + config.build.paths.pages],
                //执行的任务
                tasks: ['web_swig'],
            }
        },
        //js转化功能
        babel: {
            options: {
                sourceMap: true,//设置后会生成相应的sourceMap文件
                presets: ["@babel/preset-env"]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: config.build.src, //js目录下
                    src: [config.build.paths.scripts], //所有js文件
                    dest: config.build.temp  //输出到此目录下
                }]
            }
        },
        //sass转化
        sass: {
            dist: {
                options: {
                    sourceMap: true, //设置后会生成相应的sourceMap文件
                    implementation: sass
                },
                files: [{
                    ext:'.css',
                    expand: true,
                    cwd: config.build.src, //js目录下
                    src: [config.build.paths.styles], //所有js文件
                    dest: config.build.temp  //输出到此目录下
                }]
            }
        },
        web_swig: {
            options: {
                swigOptions: {
                    //缓存设置为false
                    cache: false
                },
                getData: function (tpl) {
                    //模板文件的数据
                    return config.data;
                }
            },
            main: {
                expand: true,
                cwd: config.build.src,//源文件文件夹
                src: config.build.paths.pages,//后缀名匹配
                dest: config.build.temp//目标文件夹
            },
        },
        copy: {
            main: {
                expand: true,
                cwd: config.build.temp,
                src: '**',
                dest: config.build.dist,
            },
            font: {
                expand: true,
                cwd: config.build.src +"/"+ config.build.paths.fonts,
                src: '**',
                dest: config.build.dist +"/"+ config.build.paths.fonts,
            }
        },
        imagemin: {
            static: {
                options: {
                    optimizationLevel: 3,
                    svgoPlugins: [{removeViewBox: false}],
                    use: [mozjpeg()] // Example plugin usage
                },
                files: {
                    //没找到咋个配置。。。。。。目前写死
                    'dist/assets/images/brands.svg': 'src/assets/images/brands.svg',
                    'dist/assets/images/logo.png': 'src/assets/images/logo.png'
                }
            },
        },
        useref: {
            // specify which files contain the build blocks
            html: config.build.dist + "/**/*.html",
            // explicitly specify the temp directory you are working in
            // this is the the base of your links ( "/" )
            temp: config.build.dist
        },
        concat: {
            basic_and_extras: {
              files: {
                'dist/assets/styles/vendor.css': ['node_modules/bootstrap/dist/css/bootstrap.css'],
                'dist/assets/scripts/vendor.js': [
                    'node_modules/jquery/dist/jquery.js',
                    'node_modules/popper.js/dist/umd/popper.js',
                    'node_modules/bootstrap/dist/js/bootstrap.js'
                ],
              },
            },
          },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        config.build.temp + '/**/*',
                    ]
                },
                options: {
                    watchTask: true,
                    ...bsInit,
                    server: {
                        baseDir: [config.build.temp, config.build.src, config.build.public],
                        routes: {
                          '/node_modules': 'node_modules'
                        }
                    }
                }
            },
            startServe:{
                options: {
                    ...bsInit,
                    server: {
                        baseDir: [config.build.dist],
                    }
                }
            }
        },
        eslint: {
            target: [config.build.src+"/"+ config.build.paths.scripts],
            css: [config.build.src+"/"+ config.build.paths.styles]
        },
        stylelint: {
            options: {
              configFile: '.stylelintrc',
              formatter: 'string',
              ignoreDisables: false,
              failOnError: true,
              outputFile: '',
              reportNeedlessDisables: false,
              syntax: 'scss'
            },
            src: [
                config.build.src+ "/"+ config.build.paths.styles
            ]
        },
        git_deploy: {
            your_target: {
              options: {
                url: config.build.giturl,
                branch: args.production || 'gh-pages',
              },
              src:  config.build.dist
            },
        },
    }
);

    //本地开发服务器启动服务和监听
    grunt.registerTask('devServe',['browserSync:dev','watch']);

    grunt.registerTask('compile',['babel','sass','web_swig',]);

    grunt.registerTask('serve',['clean','compile', 'devServe']);

    //useref

    grunt.registerTask('build',['clean','compile','imagemin','copy','useref', 'concat:basic_and_extras', 'uglify']);

    grunt.registerTask('start',['build','browserSync:startServe']);

    grunt.registerTask('lint',['stylelint','eslint']);


    grunt.registerTask('deploy',['build','git_deploy']);



    
    /**  
     * 
     *  clean   ok
        serve   ok 
        build   ok 有问题 useref 并没有处理node_modules 强行写死 感觉有点问题  font直接复制的
        start   ok
        lint    ok
        deploy   grunt-git-deploy
    */ 
}

