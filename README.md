# 简答题

### 1、谈谈你对工程化的初步认识，结合你之前遇到过的问题说出三个以上工程化能够解决问题或者带来的价值。

答：工程化根据业务特点，将前端代码开发规范化，标准化，包括： 开发流程、技术选型、代码规范、构建发布等，提升了开发效率和代码质量。

主要解决的问题： 传统语言或语法的弊端，无法使用模块化/组件化，重复的机械式工作，代码风格统一、质量保证，依赖后端接口支持，整体项目依赖后端项目



### 2、你认为脚手架除了为我们创建项目结构，还有什么更深的意义？

答： 减少了重复性且容易出错的工作、更简单创建项目



# 编程题

### 1、概述脚手架实现的过程，并使用 NodeJS 完成一个自定义的小型脚手架工具
1. 创建脚手架目录 yarn init 初始化package.json 
2. 添加bin路径
```javascirpt
{
  "name": "mybb-cli",
  "version": "1.0.0",
  "main": "index.js",
  "bin": "cli.js",//添加执行文件
  "license": "MIT",
  "dependencies": {
    "inquirer": "^7.3.3"
  }
}
```
3. 引入inquirer 模块 创建用户与命令行交互的工具 编写所需问题及字段
4. 创建cli.js 并在目录加上解释器（注意：此为node的执行目录）
```javascript
#!/usr/bin/env node //这一句是以什么方式来解析当前文件
//头部是以什么解释当前命令  
// Node cli 应用入口文件必须要有这样的文件头
// 如果liunx 或者 macos 必须给这个文件添加执行权限 755
// 具体通过 chmod 755 cli.js 实现修改

//脚手架的工作流程
// 1. 通过命令行交互询问用户问题
// 2. 根据用户回答的结果生成文件

const inquirer = require('inquirer');

inquirer.prompt([
    {
        type:"input",
        name:"name",
        message:"project name ?",
    }
]).then(answer=>{
    console.log(answer);
})
```
4. 生成项目文件
新建templates 存放需要生成的模板文件

5. 用ejs 来渲染用户输入变量 yarn add ejs 添加ejs模块

6. 完成的index.js
```javascript 
#!/usr/bin/env node
//头部是以什么解释当前命令  
// Node cli 应用入口文件必须要有这样的文件头
// 如果liunx 或者 macos 必须给这个文件添加执行权限 755
// 具体通过 chmod 755 cli.js 实现修改

//脚手架的工作流程
// 1. 通过命令行交互询问用户问题
// 2. 根据用户回答的结果生成文件

const inquirer = require('inquirer');
const path = require('path');
const fs = require("fs");
const ejs = require("ejs");

inquirer.prompt([
    {
        type:"input",
        name:"name",
        message:"project name ?",
    }
]).then(answer=>{
    console.log(answer);
    //根据用户回答的结果生成文件


    //模板文件
    const tempDir = path.join(__dirname, 'templates');

    //目标目录
    const destDir = process.cwd();


    fs.readdir(tempDir,(err,files)=>{
        if(err){
            throw err
        }
        files.forEach(file => {
            console.log(file);
            //通过模板文件渲染
            ejs.renderFile(path.join(tempDir,file),answer, (err,result) => {
                if(err) throw err;
                //写入目标目录
                fs.writeFileSync(path.join(destDir,file),result);
            })
        })
    })  
})
```

### 2、尝试使用 Gulp 完成项目的自动化构建

[详细代码]('https://gitee.com/menotwo/fed-e-task-02-01/tree/master/code/grunt') code/gulp

1. 根据项目package.json 文件发现需要实现以下
```javascript
yarn clean
yarn lint
yarn serve
yarn build
yarn start
yarn deploy
//扩展参数命令
yarn serve --port 5210 --open
```
2. 安装`gulp` 创建gulp入口文件并使用`gulp-load-plugins`管理
3.  定义目录结构
> dist						    最终发版本编译后代码目录
>
> temp						 开发时临时存储文件的目录
>
> pages.config.js        			  当前构建任务配置文件

4. 定义config对象用于管理配置定义初始路径，bsInit对象作为本地服务器端口和是否打开参数配置
	- 引入pages.config.js 文件并与config对象合并

5. 创建编译任务
    - 创建style任务处理css
      - 使用`gulp-sass`插件处理css
    - 创建script任务处理js
      - 使用`gulp-babel`处理js
    - 创建page任务处理html
      - 使用`gulp-swig`处理html 解析ejs模板 并渲染数据
      - 数据来源为config对象中的data对象(也就是从pages.config.js里配置的数据)
    - 创建image任务处理图片
      - 使用`gulp-imagemin`压缩图片
    - 创建font任务处理字体文件
      - 使用`gulp-imagemin`压缩字体文件
    - 创建extra任务把不需要编译的文件拷贝到dist目录
    - 创建clean任务用于删除dist目录与temp目录
    - 创建useref任务处理html中引入node_modules目录下的文件处理
      - 判断处理的文件类型来压缩相应代码
    - 创建publish任务把构建好的文件推送到git仓库
    	- 使用`gh-pages` 插件来推送到git仓库
    - 创建styleLint任务检查样式文件
      - 使用`csscomb`插件检查
      - 使用csscomb.json文件来约束css风格
    - 创建scriptLint任务检查js文件
      - 使用`standard`来检查js文件
6. 定义构建任务
	- 定义compile任务把css js html的构建合并为一个任务
	- 定义serve任务启动本地开发服务器
		- 本地开发端口和是否打开参数从 bsInit对象读取
		- 本地开发根路径为temp目录
		- 本地开发不用关心图片字体的处理所以添加多个路径temp,src,public 用于读取图片和字体
		- 由于引入了node_modules中的文件所以需要添加路由	
		- 需要监视项目中文件变动从而刷新浏览器，css js html需要构建才能被浏览器识别所以执行构建任务在构建任务完成时刷新浏览器
	- 定义develop任务组合compile任务和serve任务
		- 启动本地服务器之前需要先构建js css html所以执行compile任务再执行serve
	- 定义build任务
		- 执行build任务时需要先删除dist和temp目录，不删除有可能会出现文件污染
		- 清空temp和dist目录后需要先执行html js css 构建任务再执行处理node_modules文件引入问题与图片字体和不需要构建的文件拷贝任务
	- 定义startServe任务启动一个服务器 但目录为dist
		- 因为start 在构建都完成之后需要启动一个本地服务器查看构建结果所以目录为dist 
	- 定义start任务组合build和startServe
	- 定义deploy任务推代码到git仓库
		- 执行deploy任务时先执行build编译任务再推送到git仓库
	- 定义lint任务来检查代码
	  - 此任务需要检查js与css代码

7. 定义扩展方法
	- 安装`minimist`解析命令行参数，从数组第二项之后开始解析
	- 在本地服务器初始参数对象中把定义死的端口和是否打开改为用户输入的并加上默认值
	- 在推送到git任务里把分支参数改为从命令行获取并设置默认值

```javascript
// 实现这个项目的构建任务
const { src, dest, parallel, series, watch } = require('gulp')

const del = require('del')
const browserSync = require('browser-sync')
var ghpages = require('gh-pages');

const loadPlugins = require('gulp-load-plugins')
const minimist = require('minimist')
const csscomb = require('csscomb'); // css编码风格格式化
const standard = require('standard'); // js编码风格格式化


const plugins = loadPlugins()
const bs = browserSync.create()
const cwd = process.cwd();
let config = {
  //default config
  build:{
    src:'src',
    dist: 'dist',
    temp: 'temp',
    public: 'public',
    paths:{
      styles:'assets/styles/*.scss',
      scripts:'assets/scripts/*.js',
      pages:'*.html',
      images:'assets/images/**',
      fonts:'assets/fonts/**',
    }
  }
}

// 解析命令行选项
const args = minimist(process.argv.slice(2));

const bsInit = {
  notify: false,
  port: args.port || 2080,
  open: args.open || false,
};

// const data = require

try{
  const loadConfig = require(`${cwd}/pages.config.js`);
  config = Object.assign({},config,loadConfig)
} catch (err){

}


const clean = () => {
  return del([config.build.dist, config.build.temp])
}

const style = () => {
  return src(config.build.paths.styles, { base:  config.build.src ,cwd: config.build.src})
    .pipe(plugins.sass({ outputStyle: 'expanded' }))
    .pipe(dest(config.build.temp))
    .pipe(bs.reload({ stream: true }))
}

const script = () => {
  return src(config.build.paths.scripts, { base:  config.build.src , cwd: config.build.src })
    .pipe(plugins.babel({ presets: [require('@babel/preset-env')] }))
    .pipe(dest(config.build.temp))
    .pipe(bs.reload({ stream: true }))
}

const page = () => {
  return src(config.build.paths.pages, { base:  config.build.src , cwd: config.build.src })
    .pipe(plugins.swig({ data: config.data, defaults: { cache: false } })) // 防止模板缓存导致页面不能及时更新
    .pipe(dest(config.build.temp))
    .pipe(bs.reload({ stream: true }))
}

const image = () => {
  return src(config.build.paths.images, { base:  config.build.src , cwd: config.build.src })
    .pipe(plugins.imagemin())
    .pipe(dest(config.build.dist))
}

const font = () => {
  return src(config.build.paths.fonts, { base:  config.build.src , cwd: config.build.src })
    .pipe(plugins.imagemin())
    .pipe(dest(config.build.dist))
}

const extra = () => {
  return src('**', { base: config.build.public, cwd: config.build.public })
    .pipe(dest(config.build.dist))
}

const serve = () => {
  watch(config.build.paths.styles, {cwd: config.build.src},style)
  watch(config.build.paths.scripts,{cwd: config.build.src}, script)
  watch(config.build.paths.scripts,{cwd: config.build.src}, page)
  watch([
    config.build.paths.images,
    config.build.paths.fonts,
  ],{cwd: config.build.src}, bs.reload)

  watch("**",{cwd: config.build.public}, bs.reload)

  bs.init({
    ...bsInit,
    // open: false,
    // files: 'dist/**',
    server: {
      baseDir: [config.build.temp, config.build.src, config.build.public],
      routes: {
        '/node_modules': 'node_modules'
      }
    }
  })
}
//start命令开启的服务  以生产模式运行  目录则为dist
const startServe = () => {
  bs.init({
    ...bsInit,
    // open: false,
    // files: 'dist/**',
    server: {
      baseDir: [config.build.dist],
    }
  })
}




const useref = () => {
  return src('temp/*.html', { base: config.build.temp })
    .pipe(plugins.useref({ searchPath: [config.build.temp, '.'] }))
    // html js css
    .pipe(plugins.if(/\.js$/, plugins.uglify()))
    .pipe(plugins.if(/\.css$/, plugins.cleanCss()))
    .pipe(plugins.if(/\.html$/, plugins.htmlmin({
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true
    })))
    .pipe(dest(config.build.dist))
}


const publish = done =>{
  if(!config.build.giturl){
    done(new Error("请在根目录pages.config.js中配置giturl 地址"))
    return;
  }
  ghpages.publish('dist', {
    branch: args.production || 'gh-pages',
    repo: config.build.giturl
  }, function(err) {
    if(err){
      done(new Error(err))
    }else{
      done()
    }
  });
}



const styleLint = done => {
  const css = new csscomb(require('./.csscomb.json'));
  css.processPath(config.build.paths.styles);
  done();
}

const scriptLint = done => {
  standard.lintFiles(config.build.paths.scripts, { cwd: `${cwd}/src/`, fix: true }, done);
}

const lint = parallel(styleLint,scriptLint)

const outlog = done => {
  console.log(process.argv)
  console.log(minimist(process.argv.slice(2)))
  done();
}

const compile = parallel(style, script, page)

// 上线之前执行的任务
const build =  series(
  clean,
  parallel(
    series(compile, useref),
    image,
    font,
    extra
  )
)

const develop = series(compile, serve)

//start 命令之前需要先build

const start = series(build,startServe);

const deploy = series(build,publish)

module.exports = {
  clean,
  build,
  "serve":develop,
  outlog: outlog,
  start,
  deploy,
  lint
}

```



### 3、使用 Grunt 完成项目的自动化构建

- [详细代码]('https://gitee.com/menotwo/fed-e-task-02-01/tree/master/code/grunt') code/grunt
- [grunt插件地址](https://www.gruntjs.net/plugins)
- 思路大体跟gulp一致 首先查找对应功能的插件 根据插件文档 编写插件信息
  - 字体文件用copy直接复制的 
  - node_modules 文件 手动用concat拼接的
```javascript

const minimist = require('minimist') // 解析命令行选项
const sass = require('sass');
const args = minimist(process.argv.slice(2)) //解析后的命令行参数
const cwd = process.cwd(); //当前的目录
const browserSync = require("browser-sync")
const bs = browserSync.create()
const loadGruntTasks = require("load-grunt-tasks")
const mozjpeg = require('imagemin-mozjpeg')


let config = {
  //default config
  build:{
    src:'src',
    dist: 'dist',
    temp: 'temp',
    public: 'public',
    paths:{
      styles:'assets/styles/*.scss',
      scripts:'assets/scripts/*.js',
      pages:'*.html',
      images:'assets/images/**',
      fonts:'assets/fonts/',
    }
  }
}

//启动服务器配置
const bsInit = {
    notify: false,
    port: args.port || 2080,
    open: args.open || false,
};

try{
    const loadConfig = require(`${cwd}/pages.config.js`);
    config = Object.assign({},config,loadConfig)
  } catch (err){
  
}



module.exports = grunt => {
    
    loadGruntTasks(grunt) //自动加载所有的grunt插件

    grunt.initConfig({
        //清除零食目录
        clean:{
            dist: config.build.dist,
            temp: config.build.temp
        },
       
        //监听js 和 css 改变
        watch: {
            js: {
                //文件地址
                files: [config.build.src + '/' + config.build.paths.scripts],
                //执行的任务
                tasks: ['babel']
            },
            css: {
                //文件地址
                files: [config.build.src + '/' + config.build.paths.styles],
                //执行的任务
                tasks: ['sass'],
            },
            html: {
                //文件地址
                files: [config.build.src + '/' + config.build.paths.pages],
                //执行的任务
                tasks: ['web_swig'],
            }
        },
        //js转化功能
        babel: {
            options: {
                sourceMap: true,//设置后会生成相应的sourceMap文件
                presets: ["@babel/preset-env"]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: config.build.src, //js目录下
                    src: [config.build.paths.scripts], //所有js文件
                    dest: config.build.temp  //输出到此目录下
                }]
            }
        },
        //sass转化
        sass: {
            dist: {
                options: {
                    sourceMap: true, //设置后会生成相应的sourceMap文件
                    implementation: sass
                },
                files: [{
                    ext:'.css',
                    expand: true,
                    cwd: config.build.src, //js目录下
                    src: [config.build.paths.styles], //所有js文件
                    dest: config.build.temp  //输出到此目录下
                }]
            }
        },
        web_swig: {
            options: {
                swigOptions: {
                    //缓存设置为false
                    cache: false
                },
                getData: function (tpl) {
                    //模板文件的数据
                    return config.data;
                }
            },
            main: {
                expand: true,
                cwd: config.build.src,//源文件文件夹
                src: config.build.paths.pages,//后缀名匹配
                dest: config.build.temp //目标文件夹
            },
        },
        copy: {
            main: {
                expand: true,
                cwd: config.build.temp,
                src: '**',
                dest: config.build.dist,
            },
            font: {
                expand: true,
                cwd: config.build.src +"/"+ config.build.paths.fonts,
                src: '**',
                dest: config.build.dist +"/"+ config.build.paths.fonts,
            }
        },
        imagemin: {
            static: {
                options: {
                    optimizationLevel: 3,
                    svgoPlugins: [{removeViewBox: false}],
                    use: [mozjpeg()] // Example plugin usage
                },
                files: {
                    'dist/assets/images/brands.svg': 'src/assets/images/brands.svg',
                    'dist/assets/images/logo.png': 'src/assets/images/logo.png'
                }
            },
        },
        useref: {
            // specify which files contain the build blocks
            html: config.build.dist + "/**/*.html",
            // explicitly specify the temp directory you are working in
            // this is the the base of your links ( "/" )
            temp: config.build.dist
        },
        concat: {
            basic_and_extras: {
              files: {
                'dist/assets/styles/vendor.css': ['node_modules/bootstrap/dist/css/bootstrap.css'],
                'dist/assets/scripts/vendor.js': [
                    'node_modules/jquery/dist/jquery.js',
                    'node_modules/popper.js/dist/umd/popper.js',
                    'node_modules/bootstrap/dist/js/bootstrap.js'
                ],
              },
            },
          },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        config.build.temp + '/**/*',
                    ]
                },
                options: {
                    watchTask: true,
                    ...bsInit,
                    server: {
                        baseDir: [config.build.temp, config.build.src, config.build.public],
                        routes: {
                          '/node_modules': 'node_modules'
                        }
                    }
                }
            },
            startServe:{
                options: {
                    ...bsInit,
                    server: {
                        baseDir: [config.build.dist],
                    }
                }
            }
        },
        eslint: {
            target: [config.build.src+"/"+ config.build.paths.scripts],
            css: [config.build.src+"/"+ config.build.paths.styles]
        },
        stylelint: {
            options: {
              configFile: '.stylelintrc',
              formatter: 'string',
              ignoreDisables: false,
              failOnError: true,
              outputFile: '',
              reportNeedlessDisables: false,
              syntax: 'scss'
            },
            src: [
                config.build.src+ "/"+ config.build.paths.styles
            ]
        },
        git_deploy: {
            your_target: {
              options: {
                url: config.build.giturl,
                branch: args.production || 'gh-pages',
              },
              src:  config.build.dist
            },
        },
    }
);

    //本地开发服务器启动服务和监听
    grunt.registerTask('devServe',['browserSync:dev','watch']);

    grunt.registerTask('compile',['babel','sass','web_swig',]);

    grunt.registerTask('serve',['clean','compile', 'devServe']);

    //useref

    grunt.registerTask('build',['clean','compile','imagemin','copy','useref', 'concat:basic_and_extras', 'uglify']);

    grunt.registerTask('start',['build','browserSync:startServe']);

    grunt.registerTask('lint',['stylelint','eslint']);

    grunt.registerTask('deploy',['build','git_deploy']);



    
    /**  
     * 
     *  clean   ok
        serve   ok 
        build   ok
        start   ok
        lint    ok
        deploy   grunt-git-deploy
    */ 
}
```